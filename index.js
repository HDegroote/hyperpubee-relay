const DHT = require('@hyperswarm/dht')
const { relay } = require('@hyperswarm/dht-relay')
const { WebSocketServer } = require('ws')
const Stream = require('@hyperswarm/dht-relay/ws')
const Hyperswarm = require('hyperswarm')
const SwarmInterface = require('hyperpubee-swarm-interface')
const { loadConfig, DbApp } = require('hyperpubee-backend')

function setupServer ({ dht, server }) {
  server.on('error', (error) => {
    console.error(`Caught server error: ${error}`)
  })

  server.on('connection', (socket) => {
    console.log('Received connection--relaying')

    socket.on('error', (error) => {
      console.log(`caught socket error: ${error}`)
    })

    relay(dht, new Stream(false, socket))

    socket.send(
      'Hello from the relay server over a websocket (you are being relayed)'
    )
  })
}

async function setupAlwaysOnHoster ({ dht, dbApp }) {
  console.log('Loading available works into the corestore')
  const availableWorks = await dbApp.loadStoredWorksInCorestore()

  console.log('Announcing available works on the swarm')
  const swarm = new Hyperswarm(dht)
  const swarmInterface = new SwarmInterface(
    swarm,
    dbApp.hyperInterface.corestore
  )

  const discoveryKeys = availableWorks.map(
    (work) => work.bee.feed.discoveryKey
  )
  await swarmInterface.serveCores(discoveryKeys)
}

async function runRelayServer (wsPort = 8080, dhtPort) {
  const dht = new DHT({ port: dhtPort })

  const server = new WebSocketServer({ port: wsPort })
  setupServer({ dht, server })

  const config = loadConfig()
  console.log('Initialising app with config: ')
  console.log(config)

  const dbApp = await DbApp.initFromConfig(config)
  setupAlwaysOnHoster({ dht, dbApp })

  console.log('Running the relay on port', wsPort)
  console.log('Running the DHT on port', dht.port)
}

module.exports = runRelayServer
