# HyperPubee Relay

Host and relay [HyperPubees](https://gitlab.com/HDegroote/hyperpubee) over websockets so they become accessible from the browser.

## Install
`npm install hyperpubee-relay`

## Usage
```
const runRelayServer = require("hyperpubee-relay")

runRelayServer(8080) // Host the WS server on port 8080

// Alternatively, specify that the DHT should try to
// listen on some specific port rather than the default
runRelayServer(8080, 40000)
```

Note that if the DHT-port you specify is firewalled, the DHT will try to find another port.

A configuration file defines which database and corestore you are loading hyperpubees from. By default both are created in or loaded from your current folder. See [hyperpubee-backend](https://gitlab.com/HDegroote/hyperpubee-backend). 

## Functionality

A websocket server is setup which relays arbitrary hypercores over websockets. 
Furthermore, all works which are present in the database and corestore specified by the config are hosted on hyperswarm, ensuring their availability.